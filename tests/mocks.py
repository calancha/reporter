"""Test mocks."""
from unittest.mock import MagicMock

import datawarehouse

MOCK_BUILD_ID = 'redhat:123456789-x86_64-kernel'
MOCK_CHECKOUT_ID = 'redhat:123456789'
MOCK_ISSUE_ID = 4321
MOCK_TEST_ID = 'redhat:123456789-x86_64-kernel_upt_1'
MOCK_TESTRESULT_ID = 'redhat:123456789-x86_64-kernel_upt_1.5'


def get_mocked_checkout(valid=True, builds=None, issues=None, misc=None, id=MOCK_CHECKOUT_ID):
    """Return a kcidb checkout mock."""
    if builds is None:
        builds = []
    if issues is None:
        issues = []
    if misc is None:
        misc = {}
    checkout = MagicMock()
    checkout.valid = valid
    checkout.issues_occurrences = issues
    checkout.misc = misc
    checkout.id = id

    checkout.builds = builds
    return checkout


def get_mocked_build(valid=True, tests=None, issues=None, misc=None, id=MOCK_BUILD_ID):
    """Return a kcidb build mock."""
    if tests is None:
        tests = []
    if issues is None:
        issues = []
    if misc is None:
        misc = {}
    build = MagicMock()
    build.valid = valid
    build.issues_occurrences = issues
    build.misc = misc
    build.id = id

    build.tests.list = tests
    return build


def get_mocked_test(status='PASS', waived=False, issues=None, build_id=MOCK_BUILD_ID,
                    test_id=MOCK_TEST_ID):
    """Return a kcidb test mock."""
    if issues is None:
        issues = []
    test = MagicMock()
    test.status = status
    test.waived = waived
    test.issues_occurrences = issues
    test.id = test_id
    test.build_id = build_id

    return test


def get_mocked_issue_occurrence(is_regression=False, issue_attributes=None, ids=None):
    """Return a mock of an issue_occurrence."""
    if issue_attributes is None:
        issue_attributes = {}
    issue_occurrence = MagicMock()
    issue_occurrence.is_regression = is_regression
    if ids is None:
        ids = {'id': MOCK_ISSUE_ID,
               'build_id': MOCK_BUILD_ID,
               'checkout_id': MOCK_CHECKOUT_ID,
               'test_id': MOCK_TEST_ID,
               'testresult_id': MOCK_TESTRESULT_ID}
    for key, value in ids.items():
        setattr(issue_occurrence, key, value)

    issue_occurrence.issue = MagicMock()
    for key, value in issue_attributes.items():
        setattr(issue_occurrence.issue, key, value)

    return issue_occurrence


def get_mocked_test_results(test_id=MOCK_TEST_ID, status='PASS', id=MOCK_TESTRESULT_ID):
    """Return a mock of a KCIDBTestResult."""
    return MagicMock(status=status, test_id=test_id, id=id)


def get_mocked_kcidbfile(builds=None, checkouts=None, issues=None, results=None, tests=None):
    """Return a mock of a KCIDBFile."""
    if builds is None:
        builds = [get_mocked_build()]
    if checkouts is None:
        checkouts = [get_mocked_checkout()]
    if issues is None:
        issues = [get_mocked_issue_occurrence()]
    if results is None:
        results = [get_mocked_test_results()]
    if tests is None:
        tests = [get_mocked_test()]
    kcidbfile = MagicMock(spec=datawarehouse.objects.KCIDBFile)
    kcidbfile.builds = builds
    kcidbfile.checkouts = checkouts
    kcidbfile.issueoccurrences = issues
    kcidbfile.testresults = results
    kcidbfile.tests = tests

    return kcidbfile
